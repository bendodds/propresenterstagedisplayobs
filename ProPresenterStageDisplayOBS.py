""" Stage Display Viewer for OBS and Propresenter 5 and 6.

Based on stage display viewer by AronHetLam
https://obsproject.com/forum/resources/propresenter-stage-display-viewer-for-obs.720/

The principle difference is that multiple fields may be selected for
the stage display text to go into. For each field, you can select the
value that will be placed in it. CurrentSlideText, NextSlideText, etc.
"""


import obspython as obs
import re
import threading
import socket
import time

from queue import Queue
from xml.etree import ElementTree

SUCCESSFUL_LOGIN = "<StageDisplayLoginSuccess />"
SUCCESSFUL_LOGIN_WINDOWS = "<StageDisplayLoginSuccess>"
INVALID_PASSWORD = "<Error>Invalid Password</Error>"
COLOR_FILTER_NAME = "StageDisplay Viewer Transparency"

ALIGN_CENTER = "center"
ALIGN_LEFT = "left"
ALIGN_RIGHT = "right"

ALIGN_LEFT_TAG = "(L)"
ALIGN_RIGHT_TAG = "(R)"

MAX_FIELD_COUNT = 100
FIELD_COUNT = 0
FIELD_PAIRS = []

host = "localhost"
port = 50002
password = "password"
connected = False
autoconnect = True
thread_running = False  # If a thread for recieving data is running
disconnect = False  # If the thread should disconnect
disconnected = False 
last_message = None

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
queue = Queue()
thread_lock = threading.Lock()


class FieldPair(object):
    primaryFieldName = None
    secondaryFieldName = None
    visibleField = "primary"
    transitionTime = None
    valueToDisplay = None
    removeBlankLines = None

    inTransition = False
    lastText = ""
    lastAlignment = ALIGN_CENTER
    currentText = None
    currentAlignment = None
    lastUpdateStartTime = None

    def __init__(self, primaryFieldName, secondaryFieldName, 
            transitionTime, valueToDisplay, removeBlankLines,
            fallbackToCurrentSlide, hideIfEmpty):
        self.primaryFieldName = primaryFieldName
        self.secondaryFieldName = secondaryFieldName
        self.removeBlankLines = removeBlankLines
        self.fallbackToCurrentSlide = fallbackToCurrentSlide,
        self.hideIfEmpty = hideIfEmpty

        if not transitionTime:
            self.transitionTime = 0
        else:
            self.transitionTime = transitionTime

        if valueToDisplay.lower() == "video countdown":
            self.valueToDisplay = "video counter"
        else:
            self.valueToDisplay = valueToDisplay

        self.SetValue("", ALIGN_CENTER)

    def __repr__(self):
        secondaryClause = ", %s" % self.secondaryFieldName \
                if self.secondaryFieldName else ""
        return "<FieldPair %s%s for %s>" % (self.primaryFieldName, 
                secondaryClause, self.valueToDisplay)

    def SetValue(self, nextText, nextAlignment):
        if self.removeBlankLines:
            nextText = re.sub(r"^\s*$\n", "", nextText, flags = re.MULTILINE)

        if nextText == self.currentText and nextAlignment == self.currentAlignment:
            return

        self.lastText = self.currentText
        self.lastAlignment = self.currentAlignment

        self.currentText = nextText
        self.currentAlignment = nextAlignment

        self.lastUpdateStartTime = time.time()
        self.inTransition = True

        primaryField = obs.obs_get_source_by_name(self.primaryFieldName)
        secondaryField = obs.obs_get_source_by_name(self.secondaryFieldName)
        try:
            if primaryField is None:
                return

            primaryFieldFilter = obs.obs_source_get_filter_by_name(
                    primaryField, COLOR_FILTER_NAME)
            secondaryFilter = obs.obs_source_get_filter_by_name(
                    secondaryField, COLOR_FILTER_NAME)
            try:
                primaryFieldSettings = obs.obs_data_create()
                secondaryFieldSettings = obs.obs_data_create()
                primaryFieldFilterSettings = obs.obs_data_create()
                secondaryFilterSettings = obs.obs_data_create()
                try:
                    obs.obs_data_set_string(primaryFieldSettings, "text", self.currentText)
                    obs.obs_data_set_string(primaryFieldSettings, "align", self.currentAlignment)
                    if secondaryField is not None:
                        obs.obs_data_set_string(secondaryFieldSettings, "text", self.lastText)
                        obs.obs_data_set_string(secondaryFieldSettings, "align", self.lastAlignment)
                        obs.obs_data_set_int(primaryFieldFilterSettings, "opacity", 0)
                        obs.obs_data_set_int(secondaryFilterSettings, "opacity", 100)
                    else:
                        obs.obs_data_set_int(primaryFieldFilterSettings, "opacity", 0)
                    
                    obs.obs_source_update(primaryField, primaryFieldSettings)
                    obs.obs_source_update(secondaryField, secondaryFieldSettings)
                    obs.obs_source_update(primaryFieldFilter, primaryFieldFilterSettings)
                    obs.obs_source_update(secondaryFilter, secondaryFilterSettings)

                finally:
                    obs.obs_data_release(primaryFieldSettings)
                    obs.obs_data_release(secondaryFieldSettings)
                    obs.obs_data_release(primaryFieldFilterSettings)
                    obs.obs_data_release(secondaryFilterSettings)
            finally:
                obs.obs_source_release(primaryFieldFilter)
                obs.obs_source_release(secondaryFilter)
        finally:
            obs.obs_source_release(primaryField)
            obs.obs_source_release(secondaryField)
    
    def Transition(self):
        if not self.inTransition:
            return
            
        primaryField = obs.obs_get_source_by_name(self.primaryFieldName)
        try:
            if not primaryField:
                return

            time_since_last_update = time.time() - self.lastUpdateStartTime
            if self.transitionTime == 0:
                lerp = 100
            else:
                lerp = int(time_since_last_update * 100.0 / self.transitionTime)

            if self.hideIfEmpty and not self.currentText:
                primaryFieldTransparency = 0
            else:
                primaryFieldTransparency = min(100, lerp)

            primaryFieldFilter = obs.obs_source_get_filter_by_name(
                    primaryField, COLOR_FILTER_NAME)
            primaryFieldFilterSettings = obs.obs_data_create()
            try:
                obs.obs_data_set_int(primaryFieldFilterSettings, "opacity", 
                        primaryFieldTransparency)
                obs.obs_source_update(primaryFieldFilter, 
                        primaryFieldFilterSettings)

                if self.secondaryFieldName:
                    secondaryFieldTransparency = max(100 - lerp, 0)

                    secondaryField = obs.obs_get_source_by_name(self.secondaryFieldName)
                    secondaryFilter = obs.obs_source_get_filter_by_name(
                            secondaryField, COLOR_FILTER_NAME)
                    secondaryFilterSettings = obs.obs_data_create()
                    try:
                        obs.obs_data_set_int(secondaryFilterSettings, "opacity", 
                                secondaryFieldTransparency)
                        obs.obs_source_update(secondaryFilter, 
                                secondaryFilterSettings)
                    finally:
                        obs.obs_data_release(secondaryFilterSettings)
                        obs.obs_source_release(secondaryFilter)
                        obs.obs_source_release(secondaryField)
            finally:    
                obs.obs_data_release(primaryFieldFilterSettings)
                obs.obs_source_release(primaryFieldFilter)
        finally:
            obs.obs_source_release(primaryField)

        if primaryFieldTransparency >= 100:
            self.inTransition = False


def connect_button_clicked(properties, p):
    global connected
    global thread_running

    if not autoconnect and not thread_running:
        t = threading.Thread(target = connect)
        t.daemon = True
        t.start()
        queue.put(0)
        thread_running = True

    elif connected:
        print_message("Already connected")
        
    elif thread_running:
       print_message("Autoconnect running")


def connect(): #run only in thread t
    global autoconnect
    global thread_running
    global disconnect
    global connected
    global password
    global s
    global last_message

    queue.get()

    tries = 0
    while (autoconnect or tries < 1) and not disconnect:
        time.sleep(1)
        tries += 1
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((host, port))
            loginString = "<StageDisplayLogin>" + password + "</StageDisplayLogin>"
            print_message("Sending login")
            s.sendall(loginString.encode() + b'\r' + b'\n')

            data = s.recv(4096).decode("utf-8")
            print_message("Initial response from server: " + data.strip())
            if SUCCESSFUL_LOGIN_WINDOWS in data or SUCCESSFUL_LOGIN in data:
                print_message("Connected")
                with thread_lock:
                    connected = True
                while connected and not disconnect:
                    recv_and_process_data()
                s.close()
                tries = 0  # try once if connection is lost after a succesfull connection.
                print_message("Disconnected")
            elif INVALID_PASSWORD in data:
                print_message("Login to server failed: Invalid password "
                        "- Make sure the password matches the one set in "
                        "Propresenter")
            else:
                print_message("Login to server failed: Unknown response "
                        "- Make sure you're connecting to Propresenter "
                        "StageDisplay server")

        except Exception as E:
            print_message("Couldn't connect to server: " + str(E))

            s.close()
        
        with thread_lock:
            connected = False
        
    with thread_lock:
        thread_running = False


def print_message(message):
    global last_message
    if message == last_message:
        return

    last_message = message
    print(message)


def reset_pullParser():
    global pullParser
    global first

    pullParser = ElementTree.XMLPullParser(["start", "end"])
    first = True


def recv_and_process_data(): #run only in thread t
    global connected
    global pullParser
    global rootElement
    
    try:
        data = s.recv(4096).decode("utf-8")
            
    except Exception as e:
        data = ""
        if connected:
            print_message("Disconnected because of error while recieving data from server: " + str(e))
            with thread_lock:
                connected = False
        else:
            print_message("Connection was shut down")

    for line in data.splitlines(True):
        try:
            parse_and_process(line)

        except ElementTree.ParseError as e:
            reset_pullParser()
            try:
                parse_and_process(line)
            except ElementTree.ParseError as e2:
                print_message("Error parsing XML data: " + str(e2))
                reset_pullParser()


def parse_and_process(line):
    global first
    global pullParser
    global rootElement
    
    pullParser.feed(line)
    for event, element in pullParser.read_events():
        if first and event == "start":
            rootElement = element
            first = False

        if rootElement == element and event == "end":
            process_xml_data(element)
            reset_pullParser()


def process_xml_data(root):
    if root.tag != 'StageDisplayData':
        return

    for fieldPair in FIELD_PAIRS:
        nodeLabel = '**[@label="%s"]' % fieldPair.valueToDisplay
        nextSlideText = None
        nextSlideAlignment = ALIGN_CENTER
        for slide in root.findall(nodeLabel):
            if slide.text != None:
                text = slide.text.strip()
                if text.startswith(ALIGN_LEFT_TAG):
                    nextSlideAlignment = ALIGN_LEFT
                    nextSlideText = text[len(ALIGN_LEFT_TAG):]
                elif text.startswith(ALIGN_RIGHT_TAG):
                    nextSlideAlignment = ALIGN_RIGHT
                    nextSlideText = text[len(ALIGN_RIGHT_TAG):]
                else:
                    nextSlideText = text
            else:
                nextSlideText = ""

        if not nextSlideText and fieldPair.fallbackToCurrentSlide:
            nodeLabel = '**[@label="Current Slide"]'
            nextSlideText = None
            for slide in root.findall(nodeLabel):
                if slide.text != None:
                    text = slide.text.strip()
                    if text.startswith(ALIGN_LEFT_TAG):
                        nextSlideAlignment = ALIGN_LEFT
                        nextSlideText = text[len(ALIGN_LEFT_TAG):]
                    else:
                        nextSlideText = text
                else:
                    nextSlideText = ""

        with thread_lock:
            fieldPair.SetValue(nextSlideText, nextSlideAlignment)


def transition():
    global FIELD_PAIRS
    global COLOR_FILTER_NAME

    with thread_lock:
        for fieldPair in FIELD_PAIRS:
            fieldPair.Transition()


# defines script description
def script_description():
   return "Connects to Propresenter stage display server, " \
            "and sets a text source as the current slides text. " \
            "Make sure to set the right host IP, port and password, " \
            "in order to connect to Propresenter (Propresnter does't " \
            "use encryprion at all, so don't use a sensitive password " \
            "here).\n\nChoose two individual text sources to get a " \
            "fading transition.\n\nIf you don't see your text sources " \
            "in the lists, reload the script.\n\nSpecify the name of " \
            "the field on the stage display by its title"


# defines user properties
def script_properties():
    global FIELD_COUNT

    properties = obs.obs_properties_create()

    # Get list of possible text fields for selection
    possibleSources = []
    sources = obs.obs_enum_sources()
    try:
        if sources is not None:
            for source in sources:
                source_id = obs.obs_source_get_id(source)
                if source_id == "text_gdiplus" or source_id == "text_ft2_source" \
                        or source_id == "text_gdiplus_v2":
                    name = obs.obs_source_get_name(source)
                    possibleSources.append(name)
    finally:
        obs.source_list_release(sources)

    obs.obs_properties_add_text(properties, "host", "Host ip", obs.OBS_TEXT_DEFAULT)
    obs.obs_properties_add_int(properties, "port", "Port", 1, 100000, 1)
    obs.obs_properties_add_text(properties, "password", "Password", obs.OBS_TEXT_PASSWORD)
    obs.obs_properties_add_button(properties, "connect_button", "Connect to server", connect_button_clicked)
    obs.obs_properties_add_bool(properties, "autoconnect", "Automatically try to (re)connect to server")
    
    for index in range(1, FIELD_COUNT + 2):
        # Create properties
        primaryField = obs.obs_properties_add_list(
                properties, 
                "primary_field_%s" % index, 
                "Primary Field Name %s" % index, 
                obs.OBS_COMBO_TYPE_LIST, 
                obs.OBS_COMBO_FORMAT_STRING,
                )
        secondaryField = obs.obs_properties_add_list(
                properties, 
                "secondary_field_%s" % index, 
                "Secondary Field Name %s" % index, 
                obs.OBS_COMBO_TYPE_LIST, 
                obs.OBS_COMBO_FORMAT_STRING,
                )
        obs.obs_property_list_add_string(primaryField, "None", "")
        obs.obs_property_list_add_string(secondaryField, "None", "")

        for sourceName in possibleSources:
            obs.obs_property_list_add_string(primaryField, sourceName, sourceName)
            obs.obs_property_list_add_string(secondaryField, sourceName, sourceName)

        obs.obs_properties_add_float_slider(
                properties, 
                "transition_time_%s" % index, 
                "Transition time %s (s)" % index, 
                0, 
                5.0,
                0.1,
                )
        obs.obs_properties_add_text(
                properties, 
                "value_to_display_%s" % index, 
                "Field to display %s" % index, 
                obs.OBS_TEXT_DEFAULT,
                )
        obs.obs_properties_add_bool(
                properties, 
                "remove_blank_lines_%s" % index, 
                "Remove Blank Lines (%s)" % index,
                )
        obs.obs_properties_add_bool(
                properties, 
                "fallback_to_current_slide_%s" % index, 
                "Use Current Slide if Empty (%s)" % index,
                )
        obs.obs_properties_add_bool(
                properties, 
                "hide_if_empty_%s" % index, 
                "Hide Text Box if Empty (%s)" % index,
                )

    return properties


# called at startup
def script_load(settings):
    global autoconnect
    global thread_running
    global rootElement
    
    # Make the text sources show nothing at startup
    reset_pullParser()
    rootElement = None

    if autoconnect:
        t = threading.Thread(target=connect)
        t.daemon = True
        t.start()
        queue.put(0)
        thread_running = True

    obs.timer_add(transition, 25)
    

# called when unloaded
def script_unload():
    global connected
    global thread_running
    global disconnect

    #get the thread to end
    with thread_lock:
        disconnect = True
    
    #wait till the thread has closed
    while thread_running:
    	time.sleep(0.0001)

    obs.timer_remove(transition)


# called when user updatas settings
def script_update(settings):
    global host
    global port
    global password
    global autoconnect
    global thread_running

    global FIELD_PAIRS
    global FIELD_COUNT

    FIELD_PAIRS = []

    for index in range(1, MAX_FIELD_COUNT):
        primaryFieldName = obs.obs_data_get_string(settings, 
                "primary_field_%s" % index)
        secondaryFieldName = obs.obs_data_get_string(settings, 
                "secondary_field_%s" % index)

        # If only one field in a pair is specified, make it the primary one.
        if primaryFieldName is None and secondaryFieldName is not None:
            primaryFieldName = secondaryFieldName
            secondaryFieldName = None

        if primaryFieldName:
            FIELD_COUNT = index
            CreateColorCorrectionFilter(primaryFieldName, COLOR_FILTER_NAME)

        if secondaryFieldName:
            CreateColorCorrectionFilter(secondaryFieldName, COLOR_FILTER_NAME)

        transitionTime = obs.obs_data_get_double(settings, 
                "transition_time_%s" % index)
        valueToDisplay = obs.obs_data_get_string(settings, 
                "value_to_display_%s" % index)
        removeBlankLines = obs.obs_data_get_bool(settings, 
                "remove_blank_lines_%s" % index)
        fallbackToCurrentSlide = obs.obs_data_get_bool(settings, 
                "fallback_to_current_slide_%s" % index)
        hideIfEmpty = obs.obs_data_get_bool(settings, 
                "hide_if_empty_%s" % index)

        if not primaryFieldName or not valueToDisplay:
            continue

        newPair = FieldPair(primaryFieldName, secondaryFieldName, 
                transitionTime, valueToDisplay, removeBlankLines,
                fallbackToCurrentSlide, hideIfEmpty)
        FIELD_PAIRS.append(newPair)

    host = obs.obs_data_get_string(settings, "host")
    port = obs.obs_data_get_int(settings, "port")
    password = obs.obs_data_get_string(settings, "password")
    
    tmpAutoconnect = obs.obs_data_get_bool(settings, "autoconnect")
    if not autoconnect and tmpAutoconnect:
        autoconnect = tmpAutoconnect
        if not thread_running:
            t = threading.Thread(target=connect)
            t.daemon = True
            t.start()
            queue.put(0)
            thread_running = True
    else:
        autoconnect = tmpAutoconnect


def script_defaults(settings):
    obs.obs_data_set_default_double(settings, "transition_time", 0.5)
    obs.obs_data_set_default_string(settings, "host", "localhost")
    obs.obs_data_set_default_int(settings, "port", 50002)
    obs.obs_data_set_default_string(settings, "password", "password")
    obs.obs_data_set_default_bool(settings, 'autoconnect', True)


def CreateColorCorrectionFilter(sourceName, filterName):
    print("Creating filter for %r" % sourceName)
    source = obs.obs_get_source_by_name(sourceName)
    try:
        if source is None:
            return

        filter_ = obs.obs_source_get_filter_by_name(source, filterName)
        try:
            if filter_:
                return

            new_filter = obs.obs_source_create("color_filter", filterName, None, None)
            try:
                obs.obs_source_filter_add(source, new_filter)
            finally:
                obs.obs_source_release(new_filter)
        finally:
            obs.obs_source_release(filter_)

    finally:
        obs.obs_source_release(source)
